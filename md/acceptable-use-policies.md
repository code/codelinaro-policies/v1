# CodeLinaro Acceptable Use Policies

Effective date: January 1, 2021

The CodeLinaro Service is provided to you by Linaro Limited, a UK registered company number 07180318. Any reference to CodeLinaro is also a reference to Linaro Limited. 

CodeLinaro hosts a wide variety of collaborative projects from all over the world, both public and private, and that collaboration only works when users of CodeLinaro are able to work together in good faith. While using CodeLinaro, you must comply with our Acceptable Use Policies

Capitalized terms used but not defined in these Acceptable Use Policies have the meanings assigned to them in the [CodeLinaro Terms of Service](terms-of-service.md), and [CodeLinaro Privacy Statement](privacy-statement.md). 


## Compliance with Laws and Regulations

You are responsible for using the Service in a lawful manner in compliance with applicable laws, including export control regulations, not restricted to the laws of the jurisdiction where we are making the Service available to you or where you are resident, see also the [CodeLinaro Terms of Service](terms-of-service.md). See all of our other Acceptable Use Policies provided below. These policies may be updated from time to time.


## Content Restrictions

Under no circumstances will Users upload, post, host, execute, or transmit any Content to any repositories that:



* is unlawful or promotes unlawful activities;
* is or contains [sexually obscene content](codeLinaro-community-guidelines.md);
* is libelous, defamatory, or fraudulent;
* is [discriminatory or abusive](codeLinaro-community-guidelines.md) toward any individual or group;
* [gratuitously depicts or glorifies violence](codeLinaro-community-guidelines.md), including violent images;
* is or contains [false, inaccurate, or intentionally deceptive information](codeLinaro-community-guidelines.md) that is likely to adversely affect the public interest (including health, safety, election integrity, and civic participation);
* directly supports [active attack or malware campaigns](codeLinaro-community-guidelines.md) that are or would cause technical harms — such as using our platform to deliver malicious executables or as attack infrastructure, for example by organizing denial of service attacks or managing command and control servers — with no implicit or explicit dual-use purpose prior to the abuse occurring; or
* shares unauthorized product licensing keys, software for generating unauthorized product licensing keys, or software for bypassing checks for product licensing keys, including extension of a free license beyond its trial period; or
* infringes any proprietary right of any party, including patent, trademark, trade secret, copyright, right of publicity, or other right.


## Conduct Restrictions

While using the Service, under no circumstances will you:



* [harass, or abuse](codeLinaro-community-guidelines.md) any individual or group, including our employees, officers, and agents, or other users;
* [threaten, or incite violence](codeLinaro-community-guidelines.md) towards any individual or group, including our employees, officers, and agents, or other users;
* post off-topic content, or interact with platform features, in a way that significantly or repeatedly [disrupts the experience of other users](codeLinaro-community-guidelines.md);
* use our servers for any form of automated bulk activity (for example, spamming or cryptocurrency mining), to place undue burden on our servers through automated means, or to relay any form of unsolicited advertising or solicitation through our servers, such as get-rich-quick schemes;
* use our servers to disrupt or to attempt to disrupt, or to gain or to attempt to gain unauthorized access to, any service, device, data, account or network;
* [impersonate any person or entity](codeLinaro-community-guidelines.md), including any of our employees or representatives, including through false association with CodeLinaro, or by fraudulently misrepresenting your identity or site's purpose; or
* [violate the privacy of any third party](codeLinaro-community-guidelines.md), such as by posting another person's personal information without consent.

Please see the [CodeLinaro Community Guidelines](codeLinaro-community-guidelines.md) for more details.


## Spam and Inauthentic Activity on CodeLinaro

Automated bulk activity and coordinated inauthentic activity, such as spamming, are prohibited on CodeLinaro. Prohibited activities include:

* bulk distribution of promotions and advertising
* inauthentic interactions, such as fake accounts and automated inauthentic activity
* creation of or participation in secondary markets for the purpose of the proliferation of inauthentic activity
* using CodeLinaro as a platform for propagating abuse on other platforms
* phishing or attempted phishing

CodeLinaro reserves the right to interpret any action and remove any Content that is determined to be  in violation of this policy.


## Services Usage Limits

You will not reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without our express written permission.


## Information Usage Restrictions

Unless otherwise allowed by an open source or other license attached to the content hosted on the Service, you may use information from our Service for the following reasons, regardless of whether the information was scraped, collected through our API, or obtained otherwise:



* Researchers may use public, non-personal information from the Service for research purposes, only if any publications resulting from that research are [open access](https://en.wikipedia.org/wiki/Open_access).
* Archivists may use public information from the Service for archival purposes.

Scraping refers to extracting information from our Service via an automated process, such as a bot or webcrawler. Scraping does not refer to the collection of information through our API. Please see the [CodeLinaro Terms of Service](terms-of-service.md) for our [API Terms](terms-of-service.md).

You may not use information from the Service (whether scraped, collected through our API, or obtained otherwise) for spamming purposes, including for the purposes of sending unsolicited emails to users or selling User Personal Information (as defined in the [CodeLinaro Privacy Statement](privacy-statement.md)), such as to recruiters, headhunters, and job boards.

Your use of information from the Service must comply with the [CodeLinaro Privacy Statement](privacy-statement.md).


## Privacy

Misuse of User Personal Information is prohibited.

Any person, entity, or service collecting data from the Service must comply with the [CodeLinaro Privacy Statement](privacy-statement.md), particularly in regards to the collection of User Personal Information. If you collect any User Personal Information from the Service, you agree that you will only use that User Personal Information for the purpose for which that User has authorized it. You agree that you will reasonably secure any User Personal Information you have gathered from the Service, and you will respond promptly to complaints, removal requests, and "do not contact" requests from us or other users.


## Excessive Bandwidth Use

The Service's bandwidth limitations vary based on the features you use. If we determine your bandwidth usage to be significantly excessive in relation to other users of similar features, we reserve the right to suspend your Account, or otherwise limit your activity until you can reduce your bandwidth consumption.  We also reserve the right—after providing advance notice—to delete repositories that we determine to be placing undue strain on our infrastructure.


## Advertising on CodeLinaro

While we understand that you may want to promote your Content by posting supporters' names or logos in your Account, the primary focus of the Content posted in or through your Account to the Service should not be advertising or promotional marketing. This includes Content posted in all parts of the Service. You may include static images, links, and promotional text in the documentation or issues associated with projects in your Account, but they must be related to the projects you are hosting on CodeLinaro. You may not advertise in other Users' projects, such as by posting monetized or excessive bulk content in issues.

You may not promote or distribute content or activity that is illegal or otherwise prohibited by the [CodeLinaro Terms of Service](terms-of-service.md), [CodeLinaro Community Guidelines](codeLinaro-community-guidelines.md), or CodeLinaro Acceptable Use Policies, including excessive automated bulk activity (for example, spamming), get-rich-quick schemes, and misrepresentation or deception related to your promotion.

If you decide to post any promotional materials in your Account, you are solely responsible for complying with all applicable laws and regulations, including without limitation the U.S. Federal Trade Commission's Guidelines on Endorsements and Testimonials. We reserve the right to remove any promotional materials or advertisements that, in our sole discretion, violate any CodeLinaro terms or policies.


## User Protection

You must not engage in activity that harms other users. We will resolve disputes in favor of protecting users as a whole.
