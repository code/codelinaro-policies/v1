# The CodeLinaro Terms of Service

Effective date: January 1, 2021

The CodeLinaro Service is provided to you by Linaro Limited, a UK registered company number 07180318. Any reference to CodeLinaro is also a reference to Linaro Limited. 


## Definitions



1. A CodeLinaro  "Account" represents your legal relationship with Linaro Limited. A “User Account” represents an individual User’s authorization to log in to and use the Service and serves as a User’s identity on CodeLinaro. “Organizations” are shared workspaces that may be associated with a single entity or with one or more Users where multiple Users can collaborate across many projects at once. A User Account can be a member of any number of Organizations.
2. The “Agreement” refers, collectively, to all the terms, conditions, notices contained or referenced in this document (the “Terms of Service” or the "Terms") and all other operating rules, policies (including the [CodeLinaro Privacy Statement](privacy-statement.md)) and procedures that Linaro may publish from time to time on the Website. Our site policies are available here as well as our [Git repository](https://git.codelinaro.org/clo/codelinaro-policies/v1).
3. “Content” refers to content featured or displayed through the Website, including without limitation code, text, data, articles, images, photographs, graphics, software, applications, packages, designs, features, and other materials that are available on the Website or otherwise available through the Service. "Content" also includes Services. “User-Generated Content” is Content, written or otherwise, created or uploaded by our Users. "Your Content" is Content that you create or own.
4. “CodeLinaro,” “We,” and “Us” refer to Linaro Limited., as well as our affiliates, directors, subsidiaries, contractors, licensors, officers, agents, and employees.
5. The “Service” refers to the applications, software, products, and services provided by CodeLinaro, including any Beta Previews.
6. “The User,” “You,” and “Your” refer to the individual person, company, or organization that has visited or is using the Website or Service; that accesses or uses any part of the Account; or that directs the use of the Account in the performance of its functions.  A User must be at least 13 years of age. 
7. The “Website” refers to CodeLinaro’s website located at [codelinaro.org](codelinaro.org), and all content, services, and products provided by CodeLinaro at or through the Website. It also refers to CodeLinaro-owned subdomains of codelinaro.org, such as [wiki.codelinaro.org](wiki.codelinaro.org). 


## Account Terms


### Account Controls



* Users. Subject to these Terms, you retain ultimate administrative control over your User Account and the Content within it.
* Organizations. The "owner" of an Organization that was created under these Terms has ultimate administrative control over that Organization and the Content within it. Within the Service, an owner can manage User access to the Organization’s data and projects. An Organization may have multiple owners, but there must be at least one User Account designated as an owner of an Organization. If you are the owner of an Organization under these Terms, we consider you responsible for the actions that are performed on or through that Organization.


### Required Information

You must provide your real name and a valid email address in order to complete the account activation process. 


### Account Requirements

We have a few simple rules for User Accounts on CodeLinaro's Service.



* CodeLinaro is available to users in all countries, which are not excluded under United Kingdom, US, European Union or those of the country in which you are a resident, Export Control or Sanctions regulations as may be in force from time to time. However, CodeLinaro may allow persons in certain sanctioned countries or territories to access certain CodeLinaro services pursuant to specific U.S. government authorizations.
* You may not use CodeLinaro if you are or are working on behalf of a [Specially Designated National (SDN)](https://www.treasury.gov/resource-center/sanctions/SDN-List/Pages/default.aspx) or a person subject to similar blocking or denied party prohibitions administered by a U.S. government agency.
* You must be a human to create an Account. Accounts registered by "bots" or other automated methods are not permitted. We do permit machine accounts:
    * A machine account is an Account set up by an individual human who accepts the Terms on behalf of the Account, provides a valid email address, and is responsible for its actions. A machine account is used exclusively for performing automated tasks. Multiple users may direct the actions of a machine account, but the owner of the Account is ultimately responsible for the machine's actions. 
* CodeLinaro does not permit any Users under the age of 13 on our Service. If we learn of any User under the age of 13, we will [terminate that User’s Account immediately](##toc223). If you are a resident of a country where  your country’s legal minimum age for children may be older, you are responsible for complying with your country’s laws.
* Your account may only be used by one person — i.e., a single account may not be shared by multiple people. A paid Organization may only provide access to as many User Accounts as their subscription allows.


### User Account Security

You are responsible for keeping your Account secure. We provide tools such as two-factor authentication to help you maintain your Account's security, but the content of your Account and its security are up to you.



* You are responsible for all content posted and activity that occurs under your Account (even when content is posted by others who have Accounts under your Account).
* You are responsible for maintaining the security of your Account and password. CodeLinaro cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.
* You will promptly notify [CodeLinaro](https://www.codelinaro.org/contact/) if you become aware of any unauthorized use of, or access to, our Service through your Account, including any unauthorized use of your password or Account.


## Acceptable Use

Your use of the Website and Service must not violate any applicable laws, including copyright or trademark laws, export control or sanctions laws, or other laws in your jurisdiction. You are responsible for making sure that your use of the Service is in compliance with laws and any applicable regulations.

You agree that you will not under any circumstances violate the[ CodeLinaro Acceptable Use Policies](acceptable-use-policies.md) or [CodeLinaro Community Guidelines](codeLinaro-community-guidelines.md).


## User-Generated Content


#### Responsibility for User-Generated Content

You may create or upload User-Generated Content while using the Service. You are solely responsible for the content of, and for any harm resulting from, any User-Generated Content that you post, upload, link to or otherwise make available via the Service, regardless of the form of that Content. We are not responsible for any public display or misuse of your User-Generated Content.


#### CodeLinaro May Remove Content

We have the right to refuse or remove any User-Generated Content that, in our sole discretion, violates any laws or CodeLinaro terms or policies.


#### Ownership of Content, Right to Post, and License Grants

You retain ownership of and responsibility for Your Content. If you're posting anything you did not create yourself or do not own the rights to, you agree that you are responsible for any Content you post; that you will only submit Content that you have the right to post; and that you will fully comply with any third party licenses relating to Content you post.

Because you retain ownership of and responsibility for Your Content, we need you to grant us — and other CodeLinaro Users — certain legal permissions, listed below. These license grants apply to Your Content. If you upload Content that already comes with a license granting CodeLinaro the permissions we need to run our Service, no additional license is required. You understand that you will not receive any payment for any of the rights granted below. The licenses you grant to us will end when you remove Your Content from our servers, unless other Users have forked it.



1. **License Grant to CodeLinaro.**

    We need the legal right to do things like host Your Content, publish it, and share it with those you designate. You grant us and our legal successors the right to store, archive, parse, and display Your Content, and make incidental copies, as necessary in order to provide the CodeLinaro Service, including improving the Service over time. This license includes the right to do things like copy it to our database and make backups; show it to you and other users you have designated; parse it into a search index or otherwise analyze it on our servers; share it with other users you have identified; and perform it, in case Your Content is something like music or video.


    This license does not grant CodeLinaro the right to sell Your Content. It also does not grant CodeLinaro the right to otherwise distribute or use Your Content outside of our provision of the Service, except that as part of the right to archive Your Content, 

2. **License Grant to Other Users.**

    Any User-Generated Content you post publicly, including issues, comments, and contributions to other Users' repositories, may be viewed by others. By setting your repositories to be viewed publicly, you agree to allow others to view and "fork" your repositories (this means that others may make their own copies of Content from your repositories in repositories they control).


    If you set your pages and repositories to be viewed publicly, you grant each User of CodeLinaro a nonexclusive, worldwide license to use, display, and perform Your Content through the CodeLinaro Service and to reproduce Your Content solely on CodeLinaro as permitted through CodeLinaro's functionality (for example, through forking). You may grant further rights if you adopt a license. If you are uploading Content you did not create or own, you are responsible for ensuring that the Content you upload is licensed under terms that grant these permissions to other CodeLinaro Users.

3. **Contributions Under Repository License**

    Whenever you add Content to a repository containing notice of a license, you license that Content under the same terms, and you agree that you have the right to license that Content under those terms. If you have a separate agreement to license that Content under different terms, such as a contributor license agreement, that agreement will supersede.


    Isn't this just how it works already? Yep. This is widely accepted as the norm in the open-source community; it's commonly referred to by the shorthand "inbound=outbound". We're just making it explicit.

4. **Moral Rights**

    You retain all moral rights to Your Content that you upload, publish, or submit to any part of the Service, including the rights of integrity and attribution. However, you waive these rights and agree not to assert them against us, to enable us to reasonably exercise the rights granted in item 1 above, but not otherwise.


    To the extent this agreement is not enforceable by applicable law, you grant CodeLinaro the rights we need to use Your Content without attribution and to make reasonable adaptations of Your Content as necessary to render the Website and provide the Service.



## Private Repositories


### Control of Private Repositories

Some Accounts may have private repositories, which allow the User to control access to Content.


### Confidentiality of Private Repositories

CodeLinaro considers the contents of private repositories to be confidential to you. CodeLinaro will protect the contents of private repositories from unauthorized use, access, or disclosure in the same manner that we would use to protect our own confidential information of a similar nature and in no event with less than a reasonable degree of care.


### Access

CodeLinaro personnel may only access the content of your private repositories in the situations described in the [CodeLinaro Privacy Statement](privacy-statement.md). You may choose to enable additional access to your private repositories. For example:



* You may enable various CodeLinaro services or features that require additional rights to Your Content in private repositories. These rights may vary depending on the service or feature, but CodeLinaro will continue to treat your private repository Content as confidential. If those services or features require rights in addition to those we need to provide the CodeLinaro Service, we will provide an explanation of those rights.

Additionally, we may be [compelled by law](privacy-statement.md) to disclose the contents of your private repositories.

CodeLinaro will provide notice regarding our access to private repository content, unless [for legal disclosure](privacy-statement.md), to comply with our legal obligations, or where otherwise bound by requirements under law, for automated scanning, or if in response to a security threat or other risk to security.


## Copyright Infringement and DMCA Policy

If you believe that content on our website violates your copyright, please contact us in accordance with our [Digital Millennium Copyright Act Policy](dmca-takedown-policy.md). If you are a copyright owner and you believe that content on CodeLinaro violates your rights, please contact following the procedures in the [CodeLinaro DMCA Takedown Policy](dmca-takedown-policy.md).  Note that there may be legal consequences for sending a false or frivolous takedown notice. Before sending a takedown request, you must consider legal uses such as fair use and licensed uses.  Please read the policy carefully.

We reserve the right to terminate the Accounts of [infringers](codeLinaro-community-guidelines.md) of this policy.


## Intellectual Property Notice


### CodeLinaro's Rights to Content

CodeLinaro and our licensors, vendors, agents, and/or our content providers retain ownership of all intellectual property rights of any kind related to the Website and Service. We reserve all rights that are not expressly granted to you under this Agreement or by law. The look and feel of the Website and Service is copyright © CodeLinaro<sup>TM</sup>, all rights reserved. You may not duplicate, copy, or reuse any portion of the HTML/CSS, Javascript, or visual design elements or concepts without express written permission from CodeLinaro.


### CodeLinaro<sup>TM</sup> Trademarks and Logos

Prior to using  CodeLinaro’s trademarks in any way, you must first receive written permission from Linaro Limited. 


### License to CodeLinaro Policies

CodeLinaro policy documents are licensed under this [Creative Commons Zero license.](https://creativecommons.org/publicdomain/zero/1.0/) For details, see the CodeLinaro policy repository.


## API Terms

Abuse or excessively frequent requests to CodeLinaro via the API may result in the temporary or permanent suspension of your Account's access to the API. CodeLinaro, in our sole discretion, will determine abuse or excessive usage of the API. We will make a reasonable attempt to warn you via email prior to suspension.

You may not share API tokens to exceed CodeLinaro's rate limitations.

You may not use the API to download data or Content from CodeLinaro for spamming purposes, including for the purposes of selling CodeLinaro users' personal information, such as to recruiters, headhunters, and job boards.

All use of the CodeLinaro API is subject to these Terms of Service and the [CodeLinaro Privacy Statement](privacy-statement.md).

CodeLinaro may offer subscription-based access to our API for those Users who require high-throughput access or access that would result in resale of CodeLinaro's Service.


## Cancellation and Termination


### Account Cancellation

It is your responsibility to properly cancel your Account with CodeLinaro. You can cancel your Account at any time by going into your Settings in the global navigation bar at the top of the screen. The Account screen provides a simple, no questions asked cancellation link. We are not able to cancel Accounts in response to an email or phone request.


### Upon Cancellation

We will retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements, but barring legal requirements, we will delete your full profile and the Content of your repositories within 90 days of cancellation or termination (though some information may remain in encrypted backups). This information can not be recovered once your Account is cancelled.

We will not delete Content that you have contributed to other Users' repositories or that other Users have forked.

Upon request, we will make a reasonable effort to provide an Account owner with a copy of your lawful, non-infringing Account contents after Account cancellation, termination, or downgrade. You must make this request within 90 days of cancellation, termination, or downgrade.


### CodeLinaro May Terminate

CodeLinaro has the right to suspend or terminate your access to all or any part of the Website at any time, with or without cause, with or without notice, effective immediately. CodeLinaro reserves the right to refuse service to anyone for any reason at any time.


### Survival

All provisions of this Agreement which, by their nature, should survive termination will survive termination — including, without limitation: ownership provisions, warranty disclaimers, indemnity, and limitations of liability.


## Communications with CodeLinaro


### Electronic Communication Required

For contractual purposes, you (1) consent to receive communications from us in an electronic form via the email address you have submitted or via the Service; and (2) agree that all Terms of Service, agreements, notices, disclosures, and other communications that we provide to you electronically satisfy any legal requirement that those communications would satisfy if they were on paper. This section does not affect your non-waivable rights.


### Legal Notice to CodeLinaro Must Be in Writing

Communications made through email or CodeLinaro Support's messaging system will not constitute legal notice to CodeLinaro or any of its officers, employees, agents or representatives in any situation where notice to CodeLinaro is required by contract or any law or regulation. Legal notice to CodeLinaro must be in writing and served on CodeLinaro's legal agent.


### No Phone Support

CodeLinaro only offers support via email. We do not offer telephone or voice support. Please refer to [CodeLinaro Support](https://www.codelinaro.org/support/)  for any support related questions or issues.


## Disclaimer of Warranties

CodeLinaro provides the Website and the Service “as is” and “as available,” without warranty of any kind. Without limiting this, we expressly disclaim all warranties, whether express, implied or statutory, regarding the Website and the Service including without limitation any warranty of merchantability, fitness for a particular purpose, title, security, accuracy and non-infringement.

CodeLinaro does not warrant that the Service will meet your requirements; that the Service will be uninterrupted, timely, secure, or error-free; that the information provided through the Service is accurate, reliable or correct; that any defects or errors will be corrected; that the Service will be available at any particular time or location; or that the Service is free of viruses or other harmful components. You assume full responsibility and risk of loss resulting from your downloading and/or use of files, information, content or other material obtained from the Service.


## Limitation of Liability

You understand and agree that we will not be liable to you or any third party for any loss of profits, use, goodwill, or data, or for any incidental, indirect, special, consequential or exemplary damages, however arising, that result from



* the use, disclosure, or display of your User-Generated Content;
* your use or inability to use the Service;
* any modification, price change, suspension or discontinuance of the Service;
* the Service generally or the software or systems that make the Service available;
* unauthorized access to or alterations of your transmissions or data;
* statements or conduct of any third party on the Service;
* any other user interactions that you input or receive through your use of the Service; or
* any other matter relating to the Service.

Our liability is limited whether or not we have been informed of the possibility of such damages, and even if a remedy set forth in this Agreement is found to have failed of its essential purpose. We will have no liability for any failure or delay due to matters beyond our reasonable control.


## Release and Indemnification

If you have a dispute with one or more Users, you agree to release CodeLinaro from any and all claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, arising out of or in any way connected with such disputes.

You agree to indemnify us, defend us, and hold us harmless from and against any and all claims, liabilities, and expenses, including attorneys’ fees, arising out of your use of the Website and the Service, including but not limited to your violation of this Agreement, provided that CodeLinaro (1) promptly gives you written notice of the claim, demand, suit or proceeding; (2) gives you sole control of the defense and settlement of the claim, demand, suit or proceeding (provided that you may not settle any claim, demand, suit or proceeding unless the settlement unconditionally releases CodeLinaro of all liability); and (3) provides to you all reasonable assistance, at your expense.


## Changes to These Terms

We reserve the right, at our sole discretion, to amend these Terms of Service at any time and will update these Terms of Service in the event of any such amendments. We will notify our Users of material changes to this Agreement, such as price increases, at least 30 days prior to the change taking effect by posting a notice on our Website or sending email to the primary email address specified in your CodeLinaro account. Customer's continued use of the Service after those 30 days constitutes agreement to those revisions of this Agreement. For any other modifications, your continued use of the Website constitutes agreement to our revisions of these Terms of Service. You can view all changes to these Terms in the CodeLinaro site policy repository.

We reserve the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Website (or any part of it) with or without notice.


## Miscellaneous


### Governing Law

Except to the extent applicable law provides otherwise, or subject to specific contractual agreement to the contrary between you and CodeLinaro, this Agreement between you and CodeLinaro and any access to or use of the Website or the Service are governed by the laws of England and Wales.


### Non-Assignability

CodeLinaro may assign or delegate these Terms of Service and/or the CodeLinaro [Privacy Statement](privacy-statement.md), in whole or in part, to any person or entity at any time with or without your consent, including the [license grant to CodeLinaro](#bookmark=id.ycj3p2dqpan3).  You may not assign or delegate any rights or obligations under the Terms of Service or Privacy Statement without our prior written consent, and any unauthorized assignment and delegation by you is void.


### Severability, No Waiver, and Survival

If any part of this Agreement is held invalid or unenforceable, that portion of the Agreement will be construed to reflect the parties’ original intent. The remaining portions will remain in full force and effect. Any failure on the part of CodeLinaro to enforce any provision of this Agreement will not be considered a waiver of our right to enforce such provision. Our rights under this Agreement will survive any termination of this Agreement.


### Amendments; Complete Agreement

This Agreement may only be modified by a written amendment signed by an authorized representative of CodeLinaro, or by the posting by CodeLinaro of a revised version in accordance with the [Changes to These Terms](terms-of-service.html##toc235) section. Changes to These Terms. These Terms of Service, together with the CodeLinaro [Privacy Statement](privacy-statement.md), represent the complete and exclusive statement of the agreement between you and us. This Agreement supersedes any proposal or prior agreement oral or written, and any other communications between you and CodeLinaro relating to the subject matter of these terms including any confidentiality or nondisclosure agreements.


### Questions

Questions about the Terms of Service? [Contact us](https://www.codelinaro.org/contact/). 
