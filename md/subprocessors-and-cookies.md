# CodeLinaro Subprocessors and Cookies

Effective date: April 2, 2021

The CodeLinaro Service is provided to you by Linaro Limited, a UK registered company number 07180318. Any reference to CodeLinaro is also a reference to Linaro Limited. 

CodeLinaro provides a great deal of transparency regarding how we use your data, how we collect your data, and with whom we share your data. To that end, we provide this page, which details our subprocessors, and how we use cookies.


## CodeLinaro Subprocessors

When we share your information with third party subprocessors, such as our vendors and service providers, we remain responsible for it.  We work very hard to maintain your trust when we bring on new vendors, and we require all vendors to enter into data protection agreements with us that restrict their processing of Users' Personal Information as defined in our [Privacy Statement](privacy-statement.md).


<table>
  <tr>
   <td><strong>Name of Subprocessor</strong>
   </td>
   <td><strong>Description of Processing</strong>
   </td>
   <td><strong>Location of Processing</strong>
   </td>
   <td><strong>Corporate Location</strong>
   </td>
  </tr>
  <tr>
   <td>Atlassian
   </td>
   <td>StatusPage incident management communications
   </td>
   <td>United States
   </td>
   <td>United States
   </td>
  </tr>
  <tr>
   <td>AWS Amazon
   </td>
   <td>Data hosting
   </td>
   <td>United States
   </td>
   <td>United States
   </td>
  </tr>
  <tr>
  <td>Elavon
  </td>
  <td>Credit card Proccessing
  </td>
  <td> United States
  </td>
  <td>United States
  </td>
  </tr>
  <tr>
   <td>EventBridge
   </td>
   <td>XMatters critical event management
   </td>
   <td>United States
   </td>
   <td>United States
   </td>
  </tr>
  <tr>
   <td>ITMethods
   </td>
   <td>Git hosting services
   </td>
   <td>Singapore / United States
   </td>
   <td>Canada
   </td>
  </tr>
  <tr>
   <td>JFrog
   </td>
   <td>Binary data hosting services
   </td>
   <td>United States
   </td>
   <td>Israel
   </td>
  </tr>
  <tr>
   <td>Linaro
   </td>
   <td>Support ticketing system, IRC services, wiki services
   </td>
   <td>United States
   </td>
   <td>United Kingdom
   </td>
  </tr>
  <tr>
   <td>Okta
   </td>
   <td>Auth0 authentication services
   </td>
   <td>United States
   </td>
   <td>United States
   </td>
  </tr>
  <tr>
   <td>Zoho
   </td>
   <td>Mail services
   </td>
   <td>United States
   </td>
   <td>United States
   </td>
  </tr>
</table>


			

When we bring on a new subprocessor who handles our Users' Personal Information, or remove a subprocessor, or we change how we use a subprocessor, we will update this page. If you have questions or concerns about a new subprocessor, we'd be happy to help. Please contact us via Privacy contact form.


## Cookies on CodeLinaro

CodeLinaro uses cookies to provide and secure our websites, as well as to analyze the usage of our websites, in order to offer you a great user experience. Please take a look at our [Privacy Statement](privacy-statement.md) if you’d like more information about cookies, and on how and why we use them.

Since the number and names of cookies may change, the table below may be updated from time to time.


<table>
  <tr>
   <td><strong>Service Provider</strong>
   </td>
   <td><strong>Cookie Name</strong>
   </td>
   <td><strong>Description</strong>
   </td>
   <td><strong>Expiration*</strong>
   </td>
  </tr>
  <tr>
   <td>CodeLinaro Website via Auth0
   </td>
   <td>auth0.is.authenticated
   </td>
   <td>This cookie is used during a session as an indicator for the web application to know without having to continually call Auth0 service if a user has been previously authenticated
   </td>
   <td> 24 hours or until session is terminated by user
   </td>
  </tr>
  <tr>
   <td>Git
   </td>
   <td>_gitlab_session
   </td>
   <td>Though we integrate with Auth0 SSO, the GitLab service itself also handles an active user session using this cookie
   </td>
   <td>Active until session terminated by user
   </td>
  </tr>
  <tr>
   <td>Git
   </td>
   <td>known_sign_in
   </td>
   <td>For GitLab internal analytic purposes, this cookie is set to tell the service which IP and device is being used to access 
   </td>
   <td>2 weeks
   </td>
  </tr>
  <tr>
   <td>Git
   </td>
   <td>experimentation_subject_id
   </td>
   <td>This cookie is set by GitLab’s experimental framework within its core. It’s used to primarily perform A/B testing of the UI. CodeLinaro does not utilize this cookie nor does it contain any personally identifying information.
   </td>
   <td>5 hours
   </td>
  </tr>
  <tr>
   <td>ServiceDesk
   </td>
   <td>atlassian.xsrf.token
   </td>
   <td>Helps prevent XSRF attacks. Ensures that during a user's session, browser requests sent to a Jira server originated from that Jira server.
   </td>
   <td>Active until session terminated by user
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>mo.jira-sso.*
   </td>
   <td>ServiceDesk SSO plugin which allows Auth0 integration uses this for session tracking purposes
   </td>
   <td>Active until session terminated by user
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>JSESSIONID
   </td>
   <td>ServiceDesk uses this for session tracking purposes
   </td>
   <td>Active until session terminated by user
   </td>
  </tr>
  <tr>
   <td>Artifacts
   </td>
   <td>_ga
   </td>
   <td>Google Analytics for JFrog
   </td>
   <td>Active until session terminated by user
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>ACCESSTOKEN
   </td>
   <td>Issued by JFrog for user requests made to the APIs that are used by the UI
   </td>
   <td>30 minutes
   </td>
  </tr>
  <tr>
   <td>
   </td>
   <td>REFRESHTOKEN
   </td>
   <td>Issued by JFrog for user requests made to the APIs that are used by the UI
   </td>
   <td>30 minutes
   </td>
  </tr>
  <tr>
   <td>Wiki
   </td>
   <td>jwt
   </td>
   <td>Issued by Wiki for user requests made to the APIs that are used by the UI; contains scope/permissions as pertinent to said user
   </td>
   <td>30 minutes
   </td>
  </tr>
</table>


* The expiration dates for the cookies listed below generally apply on a rolling basis.

(!) Please note while we limit our use of third party cookies to those necessary to provide external functionality when rendering external content, certain pages on our website may set other third party cookies. For example, we may embed content, such as videos, from another site that sets a cookie. While we try to minimize these third party cookies, we can’t always control what cookies this third party content sets.
