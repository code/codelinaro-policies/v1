# CodeLinaro Government Takedown Policy

Effective date: April 2, 2021

The CodeLinaro Service is provided to you by Linaro Limited, a UK registered company number 07180318. Any reference to CodeLinaro is also a reference to Linaro Limited. 

At times, CodeLinaro may receive requests from governments to remove content that has been declared unlawful in their local jurisdiction. Although we may not always agree with those laws, we may need to block content if we receive a complete request from a government official so that our users in that jurisdiction may continue to have access to CodeLinaro to collaborate and build software.


## What is a complete government takedown request?

To count as a complete request, a request or notice must



* come from a relevant, official government agency
* identify illegal content
* specify the source of illegality in that jurisdiction (law or court order).

To submit a request for government takedown, please fill in the form [here](https://www.codelinaro.org/dmca/takedown/government/create/).


## What happens when we receive a complete takedown request from a government?

When we receive a notice from a relevant, official government agency that identifies illegal content and specifies the source of the illegality, we



* notify the affected users of the specific content that allegedly violates the law, and that this is a legal takedown request
* allow the affected users to appeal the decision as part of that notification
* limit the geographic scope of the takedown when possible and include that as part of the notification
* post the official request that led to the takedown in our public government takedowns repository.

Our public government takedowns repository can be found [here](https://codelinaro.org/policies/government-takedown-repo).


## Why do we publicly post takedown notices?

We are concerned about Internet censorship, and believe that transparency on a specific and ongoing level is essential to good governance. By publicly posting the notices, we can better inform the public about what content is being withheld from CodeLinaro, and why. We post takedown notices to document their potential to chill speech.

We post redacted copies of any legal notices we receive (including original notices, counter notices or retractions) at our [Government Takedown repository](https://git.codelinaro.org/dmca/takedown-notices-gov). We will not publicly publish your personal contact information; we will remove personal information (except for usernames in URLs) before publishing notices. We will not, however, redact any other information from your notice unless you specifically ask us to.

Please also note that, although we will not publicly publish unredacted notices, we may provide a complete unredacted copy of any notices we receive directly to any party whose rights would be affected by it.

## What does it mean if we post a notice in our government takedowns repository?

It means that we received the notice on the indicated date. It does not mean that the content was unlawful or wrong. It does not mean that the user identified in the notice has done anything wrong. We don't make or imply any judgment about the merit of the claims they make. We post these notices and requests only for informational purposes.
