# CodeLinaro Trademark Policy

Effective date: April 2, 2021

The CodeLinaro Service is provided to you by Linaro Limited, a UK registered company number 07180318. Any reference to CodeLinaro is also a reference to Linaro Limited. 


## What is a CodeLinaro Trademark Policy Violation?

Using a company or business name, logo, or other trademark-protected materials in a manner that may mislead or confuse others with regard to its brand or business affiliation may be considered a trademark policy violation.


## What is not a CodeLinaro Trademark Policy Violation?

Using another's trademark in a way that has nothing to do with the product or service for which the trademark was granted is not a trademark policy violation. CodeLinaro usernames are available on a first come, first served basis and may not be reserved. A CodeLinaro account with a username that happens to be the same as a registered trademark is not, by itself, necessarily a violation of our trademark policy.


## How Does CodeLinaro Respond To Reported Trademark Policy Violations?

When we receive reports of trademark policy violations from holders of federal or international trademark registrations, we review the account and may take the following actions:



* When there is a clear intent to mislead others through the unauthorized use of a trademark, CodeLinaro will suspend the account and notify the account holder.
* When we determine that an account appears to be confusing users, but is not purposefully passing itself off as the trademarked good or service, we give the account holder an opportunity to clear up any potential confusion. We may also release a username for the trademark holder's active use.


## How Do I Report a Trademark Policy Violation?

Holders of registered trademarks can report possible trademark policy violations to CodeLinaro via our [Trademark request form](/trademark/policy/report). Please submit trademark-related requests using your company email address and include all the information to help expedite our response. Also be sure to clearly describe to us why the account may cause confusion with your mark or how the account may dilute or tarnish your mark.

## Why do we publicly post takedown notices?
We are concerned about Internet censorship, and believe that transparency on a specific and ongoing level is essential to good governance. By publicly posting the notices, we can better inform the public about what content is being withheld from CodeLinaro, and why. We post takedown notices to document their potential to chill speech.

We post redacted copies of any legal notices we receive (including original notices, counter notices or retractions) at our [Trademark Takedown repository](https://git.codelinaro.org/dmca/trademark-violations). We will not publicly publish your personal contact information; we will remove personal information (except for usernames in URLs) before publishing notices. We will not, however, redact any other information from your notice unless you specifically ask us to.

Please also note that, although we will not publicly publish unredacted notices, we may provide a complete unredacted copy of any notices we receive directly to any party whose rights would be affected by it.
