# CodeLinaro Privacy Statement

Effective date: January 1, 2021

The CodeLinaro Service is provided to you by Linaro Limited, a UK registered company number 07180318. Any reference to CodeLinaro is also a reference to Linaro Limited. 

Linaro’s global privacy practices comply with the UK-GDPR (General Data Protection Regulation) and Data Protection Act 2018 (DPA) as amended (and the applicable version at the relevant time of any guidance or codes of practice in relation thereto issued by the UK Information Commissioner’s Office from time to time); and the US, European GDPR or alternative jurisdiction’s equivalent legislation, as amended from time to time, where relevant to our places of business and supply of Service.


All capitalized terms have their definition in [CodeLinaro’s Terms of Service](terms-of-service.md), unless otherwise noted here.


## What information CodeLinaro collects


### User Personal Information

"User Personal Information" is any information about one of our Users which could, alone or together with other information, personally identify them or otherwise be reasonably linked or connected with them. Information such as a username and password, an email address, a real name, an Internet protocol (IP) address, and a photograph are examples of “User Personal Information.”

User Personal Information does not include aggregated, non-personally identifying information that does not identify a User or cannot otherwise be reasonably linked or connected with them. We may use such aggregated, non-personally identifying information for research purposes and to operate, analyze, improve, and optimize our Website and Service.


## Information users provide directly to CodeLinaro


### Registration information

We gather some basic information at the time of account creation. We ask you for your full name, corporate affiliation and a valid email address.


### Profile information

You may choose to give us more information for your Account profile, such as a display name, and an avatar which may include a photograph. This information may include User Personal Information. Please note that your profile information may be visible to other Users of our Service.


## Automatically collected information


### Usage information

If you're accessing our Service or Website, we automatically collect the same basic information that most services collect, subject, where necessary, to your consent. This includes information about how you use the Service, such as the pages you view, the referring site, your IP address and session information, and the date and time of each request. This is information we collect from every visitor to the Website, whether they have an Account or not. This information may include User Personal information.


### Device information

We may collect certain information about your device, such as its IP address, browser or client application information, language preference, operating system and application version, device type and ID, and device model and manufacturer. This information may include User Personal information.


### Cookies

As further described below, we automatically collect information from cookies (such as cookie ID and settings) to keep you logged in, to remember your preferences, to identify you and your device and to analyze your use of our service.


## Information we collect from third parties

CodeLinaro may collect User Personal Information from our third party partners.  CodeLinaro does not purchase User Personal Information from third-party data brokers.


## What information CodeLinaro does not collect

We do not intentionally collect “[Sensitive Personal Information](https://gdpr-info.eu/art-9-gdpr/)”, such as personal data revealing racial or ethnic origin, political opinions, religious or philosophical beliefs, or trade union membership, and the processing of genetic data, biometric data for the purpose of uniquely identifying a natural person, data concerning health or data concerning a natural person’s sex life or sexual orientation. If you choose to store any Sensitive Personal Information on our servers, you are responsible for complying with any regulatory controls regarding that data.

If you are a child under the age of 13, you may not have an Account on CodeLinaro. CodeLinaro does not knowingly collect information from or direct any of our content specifically to children under 13. If we learn or have reason to suspect that you are a User who is under the age of 13, we will have to close your Account. We don't want to discourage you from learning to code, but those are the rules. Please see the [CodeLinaro Terms of Service](terms-of-service.md) for information about Account termination. Different countries may have different minimum age limits, and if you are below the minimum age for providing consent for data collection in your country, you may not have an Account on CodeLinaro.

We do not intentionally collect User Personal Information that is stored in any repositories or other free-form content inputs. Any personal information within a repository is the responsibility of the repository owner.


## How CodeLinaro uses your information

We may use your information for the following purposes:



* We use your Registration Information to create your account, and to provide you the Service.
* We use your User Personal Information, specifically your full name, to generate a unique username which we use to identify you on CodeLinaro.
* We use your Profile Information to fill out your Account profile and to share that profile with organizations you belong to.
* We use your email address to communicate with you, if you've said that's okay, and only for the reasons you’ve said that’s okay. Please see our section on email communication for more information.
* We use User Personal Information to respond to support requests.
* We may use User Personal Information to invite you to take part in surveys, beta programs, or other research projects, subject, where necessary, to your consent.
* We use Usage Information and Device Information to better understand how our Users use CodeLinaro and to improve our Website and Service.
* We may use your User Personal Information if it is necessary for security purposes or to investigate possible fraud or attempts to harm CodeLinaro or our Users.
* We may use your User Personal Information to comply with our legal obligations, protect our intellectual property, and enforce the [CodeLinaro Terms of Service](terms-of-service.md).
* We limit our use of your User Personal Information to the purposes listed in this Privacy Statement. If we need to use your User Personal Information for other purposes, we will ask your permission first. You can always see what information we have, how we're using it, and what permissions you have given us in your user profile.


## Our legal bases for processing information

To the extent that our processing of your User Personal Information is subject to certain international laws (including, but not limited to, the European Union's General Data Protection Regulation (GDPR)), CodeLinaro is required to notify you about the legal basis on which we process User Personal Information. CodeLinaro processes User Personal Information on the following legal bases:


### Contract Performance

When you create or are provided a CodeLinaro Account, you provide your Registration Information. We require this information for you to enter into the [Terms of Service](terms-of-service.md) agreement with us, and we process that information on the basis of performing that contract. We also process your username and email address on other legal bases, as described below.


### Consent

We rely on your consent to use your User Personal Information under the following circumstances: when you fill out the information in your [user profile](codelinaro.org/secure/settings/); when you decide to participate in a CodeLinaro training, research project, beta program, or survey; and for marketing purposes, where applicable. All of this User Personal Information is entirely optional, and you have the ability to access, modify, and delete it at any time. While you are not able to delete your email address entirely, you can make it private. You may withdraw your consent at any time.


### Legitimate Interests

Generally, the remainder of the processing of User Personal Information we perform is necessary for the purposes of our legitimate interest, for example, for legal compliance purposes, security purposes, or to maintain ongoing confidentiality, integrity, availability, and resilience of CodeLinaro’s systems, Website, and Service.

If you would like to request deletion of data we process on the basis of consent or if you object to our processing of personal information, please [contact](https://codelinaro.org/contact) requesting Personal/Private Information requests.


## How we share the information we collect

We may share your User Personal Information with third parties under one of the following circumstances:


### With your consent

We share your User Personal Information, if you consent, after letting you know what information will be shared, with whom, and why. 


### With service providers

We share User Personal Information with a limited number of service providers who process it on our behalf to provide or improve our Service, and who have agreed to privacy restrictions similar to the ones in our Privacy Statement by signing data protection agreements or making similar commitments. Our service providers perform customer support ticketing, network data transmission, security, and other similar services. While CodeLinaro processes all User Personal Information in the United States, our service providers may process data outside of the United States or the European Union. If you would like to know who our service providers are, please see our page on [subprocessors](subprocessors-and-cookies.md).


### For security purposes

If you are a member of an Organization, CodeLinaro may share your username, Usage Information, and Device Information associated with that Organization with an owner and/or administrator of the Organization, to the extent that such information is provided only to investigate or respond to a security incident that affects or compromises the security of that particular Organization.


### For legal disclosure

CodeLinaro strives for transparency in complying with legal process and legal obligations. Unless prevented from doing so by law or court order, or in rare, exigent circumstances, we make a reasonable effort to notify users of any legally compelled or required disclosure of their information. CodeLinaro may disclose User Personal Information or other information we collect about you to law enforcement if required in response to a valid subpoena, court order, search warrant, a similar government order, or when we believe in good faith that disclosure is necessary to comply with our legal obligations, to protect our property or rights, or those of third parties or the public at large.


### Change in control or sale

We may share User Personal Information if we are involved in a merger, sale, or acquisition of corporate entities or business units. If any such change of ownership happens, we will ensure that it is under terms that preserve the confidentiality of User Personal Information, and we will notify you on our Website or by email before any transfer of your User Personal Information. The organization receiving any User Personal Information will have to honor any promises we made in our Privacy Statement or [Terms of Service](terms-of-service.md).


### Aggregate, non-personally identifying information

We share certain aggregated, non-personally identifying information with others about how our users, collectively, use CodeLinaro, or how our users respond to our other offerings, such as our conferences or events.

We **do not** sell your User Personal Information for monetary or other consideration.


## Repository contents


### Access to private repositories

If your repository is private, you control the access to your Content. If you include User Personal Information or Sensitive Personal Information, that information may only be accessible to CodeLinaro in accordance with this Privacy Statement. CodeLinaro personnel do not access private repository content except for


* security purposes
* to assist the repository owner with a support matter
* to maintain the integrity of the Service
* to comply with our legal obligations
* if we have reason to believe the contents are in violation of the law, or
* with your consent.

However, while we do not generally search for content in your repositories, we may scan our servers and content to detect certain tokens or security signatures, known active malware, known vulnerabilities in dependencies, or other content known to violate our Terms of Service, such as violent extremist or terrorist content or child exploitation imagery, based on algorithmic fingerprinting techniques (collectively, "automated scanning"). Our Terms of Service provides more [details on private repositories](terms-of-service.md).

CodeLinaro will provide notice regarding our access to private repository content, unless for legal disclosure, to comply with our legal obligations, or where otherwise bound by requirements under law, for automated scanning, or if in response to a security threat or other risk to security.


### Public repositories

If your repository is public, anyone may view its contents. If you include User Personal Information, [Sensitive Personal Information](https://gdpr-info.eu/art-9-gdpr/), or confidential information, such as email addresses or passwords, in your public repository, that information may be indexed by search engines or used by third parties.

Please see more about User Personal Information in public repositories [User Personal Information in public repositories](#heading=h.tuwsxeroe16w).


## Other important information


### Public information on CodeLinaro

Many of CodeLinaro's services and features are public-facing. If your content is public-facing, third parties may access and use it in compliance with our [Terms of Service](terms-of-service.md), such as by viewing your profile or repositories or pulling data via our API. We do not sell that content; it is yours. However, we do allow third parties, such as research organizations or archives, to compile public-facing CodeLinaro information. Other third parties, such as data brokers, have been known to scrape CodeLinaro and compile data as well.

Your User Personal Information associated with your content could be gathered by third parties in these compilations of CodeLinaro data. If you do not want your User Personal Information to appear in third parties’ compilations of CodeLinaro data, please do not make your User Personal Information publicly available and be sure to configure your email address to be private in your user profile and in your git commit settings. We currently set Users' email address to private by default.

If you would like to compile CodeLinaro data, you must comply with our Terms of Service regarding [information usage and privacy](terms-of-service.md), and you may only use any public-facing User Personal Information you gather for the purpose for which our user authorized it. For example, where a CodeLinaro user has made an email address public-facing for the purpose of identification and attribution, do not use that email address for the purposes of sending unsolicited emails to users or selling User Personal Information, such as to recruiters, headhunters, and job boards, or for commercial advertising. We expect you to reasonably secure any User Personal Information you have gathered from CodeLinaro, and to respond promptly to complaints, removal requests, and "do not contact" requests from CodeLinaro or CodeLinaro users.

Similarly, projects on CodeLinaro may include publicly available User Personal Information collected as part of the collaborative process. If you have a complaint about any User Personal Information on CodeLinaro, please see our section on resolving complaints.


## How you can access and control the information we collect

If you're already a CodeLinaro user, you may access, update, alter, or delete your basic user profile information by editing your user profile or contacting CodeLinaro Support. You can control the information we collect about you by limiting what information is in your profile, by keeping your information current, or by contacting [CodeLinaro Support](https://www.codelinaro.org/support/).

If CodeLinaro processes information about you, such as information CodeLinaro receives from third parties, and you do not have an account, then you may, subject to applicable law, access, update, alter, delete, or object to the processing of your personal information by contacting [CodeLinaro Support](https://www.codelinaro.org/support/).


### Data retention and deletion of data

Generally, CodeLinaro retains User Personal Information for as long as your account is active or as needed to provide you services.

If you would like to cancel your account, you may do so by contacting your administrator. We retain and use your information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements, but barring legal requirements, we will delete your full profile (within reason) within 90 days of your request. 

After an account has been deleted, certain data, such as contributions to other Users' repositories and comments in others' issues, will remain. However, we will delete or de-identify your User Personal Information, including your username and email address, from the author field of issues, pull requests, and comments by associating them with a [ghost user](https://codelinaro.org/ghost).

That said, the email address you have supplied via your Git commit settings will always be associated with your commits in the Git system. If you choose to make your email address private, you should also update your Git commit settings. We are unable to change or delete data in the Git commit history — the Git software is designed to maintain a record — but we do enable you to control what information you put in that record.


## Our use of cookies and tracking


### Cookies

CodeLinaro only uses strictly necessary cookies. Cookies are small text files that websites often store on computer hard drives or mobile devices of visitors.

We use cookies solely to provide, secure, and improve our service. For example, we use them to keep you logged in, remember your preferences, identify your device for security purposes, analyze your use of our service, compile statistical reports, and provide information for future development of CodeLinaro. At times we use our own cookies for analytics purposes.

By using our service, you agree that we can place these types of cookies on your computer or device. If you disable your browser or device’s ability to accept these cookies, you will not be able to log in or use our service.

We provide more information about cookies on CodeLinaro on our [CodeLinaro Subprocessors and Cookies](subprocessors-and-cookies.md) page that describes the cookies we set, the needs we have for those cookies, and the expiration of such cookies.


## How CodeLinaro secures your information

CodeLinaro takes all measures reasonably necessary to protect User Personal Information from unauthorized access, alteration, or destruction; maintain data accuracy; and help ensure the appropriate use of User Personal Information.

CodeLinaro enforces a written security information program. Our program:

* aligns with industry recognized frameworks;
* includes security safeguards reasonably designed to protect the confidentiality, integrity, availability, and resilience of our Users' data;
* is appropriate to the nature, size, and complexity of CodeLinaro’s business operations;
* includes incident response and data breach notification processes; and
* complies with applicable information security-related laws and regulations in the geographic regions where CodeLinaro does business and supplies the Service.

In the event of a data breach that affects your User Personal Information, we will act promptly to mitigate the impact of a breach and notify any affected Users without undue delay.

Transmission of data on CodeLinaro is encrypted using SSH, HTTPS (TLS), and all data is encrypted at rest. When data is stored with a third-party storage provider, it is encrypted.


## CodeLinaro's global privacy practices

CodeLinaro/Linaro’s global privacy practices comply with the UK-GDPR (General Data Protection Regulation) and General Data Protection Act 2018 (DPA) as amended (and the applicable version at the relevant time of any guidance or codes of practice in relation thereto issued by the Information Commissioner’s Office from time to time); and the US, European GDPR or alternative jurisdiction’s equivalent legislation where relevant to our places of business and supply of the Service.

Linaro Limited is the controller responsible for the processing of your personal information in connection with the Service, except (a) with respect to personal information that was added to a repository by its contributors, in which case the owner of that repository is the controller and CodeLinaro is the processor (or, if the owner acts as a processor, CodeLinaro will be the subprocessor); or (b) when you and CodeLinaro have entered into a separate agreement that covers data privacy (such as a Data Processing Agreement).

Our address is:

* LinaroLimited, Harston Mill,Harston, Cambridge CB22 7GG, UK

We store and process the information that we collect in the United States in accordance with this Privacy Statement, though our service providers may store and process data outside the United States. However, we understand that we have Users from different countries and regions with different privacy expectations, and we try to meet those needs even when the United States does not have the same privacy framework as other countries.

We provide the same high standard of privacy protection—as described in this Privacy Statement—to all our users around the world, regardless of their country of origin or location, and we are proud of the levels of notice, choice, accountability, security, data integrity, access, and recourse we provide. If our vendors or affiliates have access to User Personal Information, they are required to adhere to equivalent privacy policies and with applicable data privacy laws.

In particular:

* CodeLinaro provides clear methods of unambiguous, informed, specific, and freely given consent at the time of data collection, when we collect your User Personal Information using consent as a basis.
* We collect only the minimum amount of User Personal Information necessary for our purposes, unless you choose to provide more. We encourage you to only give us the amount of data you are comfortable sharing.
* We offer you simple methods of accessing, altering, or deleting the User Personal Information we have collected, where legally permitted.
* We provide our Users notice, choice, accountability, security, and access regarding their User Personal Information, and we limit the purpose for processing it. We also provide our Users a method of recourse and enforcement.


### Cross-border data transfers

CodeLinaro processes personal information both inside and outside of the United States and relies on Standard Contractual Clauses as the legally provided mechanism to lawfully transfer data from the European Economic Area, the United Kingdom, and Switzerland to the United States.


## How CodeLinaro communicates with you

We use your email address to communicate with you. 


## Resolving complaints

If you have concerns about the way CodeLinaro is handling your User Personal Information, please let us know immediately. Contact us [here](https://www.codelinaro.org/contact/).


### Dispute resolution process

In the unlikely event that a dispute arises between you and CodeLinaro regarding our handling of your User Personal Information, we will do our best to resolve it. 


## Changes to our Privacy Statement

Although most changes are likely to be minor, CodeLinaro may change our Privacy Statement from time to time. We will provide notification to Users of material changes to this Privacy Statement through the CodeLinaro website.


## License

This Privacy Statement is licensed under this Creative Commons Zero license. For details, see our site-policy repository.


## Contact CodeLinaro about Privacy Concerns

Questions regarding CodeLinaro's Privacy Statement or information practices may be posed by contacting us by email at [contact@codelinaro.org](mailto:contact@codelinaro.org) with the subject line "Privacy Concerns.”


## Contacting CodeLinaro

For other questions contact CodeLinaro [here](https://www.codelinaro.org/contact/).
