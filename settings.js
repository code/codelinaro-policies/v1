const mdFileOrderSettings = {
  "terms-of-service.md": 1,
  "codeLinaro-community-guidelines.md": 2,
  "acceptable-use-policies.md": 3,
  "trademark-policy.md": 4,
  "subprocessors-and-cookies.md": 5,
  "dmca-takedown-policy.md": 6,
  "guide-to-submitting-a-dmca-takedown-notice.md": 7,
  "guide-to-submitting-a-dmca-counter-notice.md": 8,
};

exports.mdFileOrderSettings = mdFileOrderSettings;
